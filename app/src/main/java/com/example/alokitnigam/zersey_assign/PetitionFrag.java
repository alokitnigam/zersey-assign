package com.example.alokitnigam.zersey_assign;


import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class PetitionFrag extends Fragment {
    EditText title, desc,category;
    TextView image;
    String uuid=UUID.randomUUID().toString();;
    private static final int PICK_IMAGE_REQUEST = 100;
    ImageView imageView;
    Button submit;
    Bitmap bitmap=null;
    private FirebaseAuth mAuth;
    private StorageReference storageReference;
    private StorageReference mStorageRef;
    String uploaded_image_url;


    private Uri filePath;


    public PetitionFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_petition, container, false);
        title = v.findViewById(R.id.title1);
        desc = v.findViewById(R.id.desc1);
        image = v.findViewById(R.id.image);
        category = v.findViewById(R.id.categoy1);
        imageView=v.findViewById(R.id.image1);
        imageView.setVisibility(View.INVISIBLE);
        submit=v.findViewById(R.id.submit);
        mAuth = FirebaseAuth.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance().getReference();
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, PICK_IMAGE_REQUEST);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });




        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICK_IMAGE_REQUEST:
                if (resultCode == RESULT_OK) {


                    // method
                    filePath = data.getData();
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                        this.bitmap= bitmap;
                        imageView.setImageBitmap(bitmap);
                        imageView.setVisibility(View.VISIBLE);
                        image.setVisibility(View.INVISIBLE);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                }
                break;
        }


    }
    void saveData(){

        if(title.getText().toString().equalsIgnoreCase("")&&desc.getText().toString().equalsIgnoreCase("")
                &&category.getText().toString().equalsIgnoreCase("")&& bitmap == null){
            Toast.makeText(getActivity(),"Fields cannot be empty",Toast.LENGTH_LONG).show();
        }
        else{
            uploadImage();

        }


    }
    private String getFileExtension(Uri uri){
        ContentResolver contentResolver=getActivity().getContentResolver();
        MimeTypeMap mimeTypeMap=MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    private void uploadImage() {

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            final StorageReference ref = storageReference.child("images/"+uuid+"."+getFileExtension(filePath));

            UploadTask uploadTask= ref.putFile(filePath);

            Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }

                    // Continue with the task to get the download URL
                    return ref.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()) {
                        Uri downloadUri = task.getResult();
                        uploaded_image_url = downloadUri.toString();
                        Log.v("jkasckhascajk",uploaded_image_url);

                        savedetails(downloadUri.toString());
                        progressDialog.dismiss();

                    } else {
                        uploaded_image_url = "";
                        Toast.makeText(getActivity(), "Image uploading failed ", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }
            });

        }
    }

    private void savedetails(String uploaded_image_url) {

        Map<String, Object> petition = new HashMap<>();
        petition.put("Title", title.getText().toString());
        petition.put("Description", desc.getText().toString());
        petition.put("Category", category.getText().toString());
        petition.put("ImagePath",uploaded_image_url);
        petition.put("Created By",mAuth.getCurrentUser().getUid());
        petition.put("Likes",new ArrayList<>());
        petition.put("Comment",0);


        FirebaseFirestore db = FirebaseFirestore.getInstance();

// Add a new document with a generated ID
        db.collection("Petitions")
                .add(petition)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }
}
