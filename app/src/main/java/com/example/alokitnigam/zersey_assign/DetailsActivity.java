package com.example.alokitnigam.zersey_assign;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import static android.content.ContentValues.TAG;

public class DetailsActivity extends AppCompatActivity {
    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    EditText comment;
    RecyclerView rv;
     PetitionModel model;
    ArrayList<CommentModel> list=new ArrayList<>();
    CommentsAdapter adapter;
    long totalcomments=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
         model=(PetitionModel) getIntent().getSerializableExtra("data");
        getSupportActionBar().setTitle(model.getTitle());

        ImageView imageView=findViewById(R.id.htab_header);
        TextView desc=findViewById(R.id.desc);
        comment=findViewById(R.id.comment);
        rv=findViewById(R.id.rv);

        rv.setLayoutManager(new LinearLayoutManager(this));
         adapter=new CommentsAdapter(this,list);
        rv.setAdapter(adapter);
        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (comment.getText().length()!=0)
                {
                    Map<String, Object> petition = new HashMap<>();
                    petition.put("comment", comment.getText().toString());
                    petition.put("time", System.currentTimeMillis());
                    petition.put("byUser", FirebaseAuth.getInstance().getCurrentUser().getUid());

                    db.collection("Petitions").document(model.getId())
                            .collection("Comments")
                            .add(petition).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                            updatecomment();
                        }
                    })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });;

                }
            }
        });

        desc.setText(model.getDescription());
        Glide.with(this).load(model.getImagepath())
                .into(imageView);

        db.collection("Petitions").document(model.getId()).collection("Comments")
                .orderBy("time",Query.Direction.DESCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        list.clear();
                        List<DocumentSnapshot> lista=queryDocumentSnapshots.getDocuments();
                        for (int i = 0; i < lista.size(); i++) {
                            CommentModel commentModel=new CommentModel();
                            commentModel.setId(lista.get(i).getId());
                            commentModel.setComment(lista.get(i).get("comment")+"");
                            commentModel.setName(lista.get(i).get("byUser")+"");
                            commentModel.setTime(lista.get(i).get("time")+"");
                            list.add(commentModel);
                        }
                        adapter.notifyDataSetChanged();
                    }
                });


        db.collection("Petitions").document(model.getId())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        Log.i("", "onEvent: "+documentSnapshot.getData());
                        totalcomments=(long) documentSnapshot.get("Comment");
                    }
                });

    }

    private void updatecomment() {
        db.collection("Petitions").document(model.getId()).update("Comment",totalcomments+1);

    }
}
