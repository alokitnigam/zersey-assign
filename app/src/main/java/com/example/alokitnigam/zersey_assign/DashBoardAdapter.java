package com.example.alokitnigam.zersey_assign;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.ViewHolder> {
    Context context;
    ArrayList<PetitionModel> list;
    private StorageReference mStorageRef;
    Bitmap bitmap;

    FirebaseAuth mAuth;
    DashBoardAdapter(Context c, ArrayList list){
        this.list=list;
        this.context=c;
        mStorageRef = FirebaseStorage.getInstance().getReference("images");
        mAuth = FirebaseAuth.getInstance();
    }
    @NonNull
    @Override
    public DashBoardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboardadapter,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DashBoardAdapter.ViewHolder holder, int position) {

        holder.title.setText(list.get(position).getTitle());
        Glide.with(context).load(list.get(position).getImagepath())
                .into(holder.image);


        if (list.get(position).isUserLiked())
        {
            holder.like.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_liked));
        }
        else
        {
            holder.like.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_like));

        }

        holder.totallikes.setText(list.get(position).getTotalLikes()+" Likes");
        holder.comments.setText(list.get(position).getTotalComments()+" Comments");


        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference likeref = db.collection("Petitions").document(list.get(holder.getAdapterPosition()).getId());

                if (list.get(holder.getAdapterPosition()).isUserLiked())
                {
                    likeref.update("Likes", FieldValue.arrayRemove(mAuth.getCurrentUser().getUid()));
                    list.get(holder.getAdapterPosition()).setUserLiked(false);
                    holder.like.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_like));
                    list.get(holder.getAdapterPosition()).setTotalLikes(list.get(holder.getAdapterPosition()).getTotalLikes()-1);
                    holder.totallikes.setText((list.get(holder.getAdapterPosition()).getTotalLikes())+" Likes");
                }
                else
                {
                    likeref.update("Likes", FieldValue.arrayUnion(mAuth.getCurrentUser().getUid()));
                    list.get(holder.getAdapterPosition()).setUserLiked(true);
                    holder.like.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_liked));
                    list.get(holder.getAdapterPosition()).setTotalLikes(list.get(holder.getAdapterPosition()).getTotalLikes()+1);
                    holder.totallikes.setText((list.get(holder.getAdapterPosition()).getTotalLikes())+" Likes");

                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder{
        ImageView image,like,comment;

        TextView title,totallikes,comments;
        public ViewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageView);
            like=itemView.findViewById(R.id.like);
            comment=itemView.findViewById(R.id.comment1);
            title=itemView.findViewById(R.id.title);
            totallikes=itemView.findViewById(R.id.totallikes);
            comments=itemView.findViewById(R.id.comments);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context,DetailsActivity.class).putExtra("data",list.get(getAdapterPosition())));
                }
            });
        }

    }

}
