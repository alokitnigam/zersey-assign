package com.example.alokitnigam.zersey_assign;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFrag extends Fragment {
    DashBoardAdapter adapter;
    RecyclerView recyclerView;
    ArrayList<PetitionModel> list=new ArrayList<>();
    final FirebaseFirestore db = FirebaseFirestore.getInstance();
    final String TAG = SignupActivity.class.getName();
    private StorageReference mStorageRef;


    FirebaseAuth mAuth;


    public DashBoardFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_dash_board, container, false);
        recyclerView=v.findViewById(R.id.rv);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        adapter=new DashBoardAdapter(getActivity(),list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        mAuth = FirebaseAuth.getInstance();

        getData();

        return v;
    }


    void getData(){

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Getting Petitions");
//        progressDialog.show();

//        db.collection("Petitions")
//                .get()
//                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
//                    @Override
//                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
//                        if (task.isSuccessful()) {
//                            for (QueryDocumentSnapshot document : task.getResult()) {
//                                Log.d("------------------->", document.getData().get("Category")+"");
//                                PetitionModel petitionModel=new PetitionModel();
//
//                                petitionModel.setId(document.getId());
//                                petitionModel.setTitle(document.getData().get("Title")+"");
//                                petitionModel.setCategory(document.getData().get("Category")+"");
//                                petitionModel.setCreatedby(document.getData().get("Created By")+"");
//                                petitionModel.setImagepath(document.getData().get("ImagePath")+"");
//                                petitionModel.setDescription(document.getData().get("Description")+"");
//
//                                Log.i(TAG, "onComplete: "+document.getData().get("Likes"));
//                                if (!(document.getData().get("Likes")+"").replaceAll("\\[","")
//                                        .replaceAll("\\]","").equals(""))
//                                {
//                                    String[] likes=(document.getData().get("Likes")+"").replaceAll("\\[","")
//                                            .replaceAll("\\]","")
//                                            .split(",");
//                                    petitionModel.setLikelist(Arrays.asList(likes));
//                                }
//                                else
//                                    petitionModel.setLikelist(new ArrayList<String>());
//
//
//                                petitionModel.setUserLiked(petitionModel.getLikelist().contains(mAuth.getCurrentUser().getUid()));
//                                petitionModel.setTotalLikes(petitionModel.getLikelist().size());
//                                list.add(petitionModel);
//                                document.getData().get("Category");
//
//
//                            }
//                            adapter.notifyDataSetChanged();
//                            Log.v("hjkfsvvhdv",list.size()+"");
//
//
//                        } else {
//                            Log.w("------------------->", "Error getting documents.", task.getException());
//                        }
//                        progressDialog.dismiss();
//
//                    }
//                });

        db.collection("Petitions").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                List<DocumentSnapshot> lista=queryDocumentSnapshots.getDocuments();
                list.clear();
                for (int i = 0; i < lista.size(); i++) {
                    PetitionModel petitionModel=new PetitionModel();

                    petitionModel.setId(lista.get(i).getId());
                    petitionModel.setTitle(lista.get(i).getData().get("Title")+"");
                    petitionModel.setCategory(lista.get(i).getData().get("Category")+"");
                    petitionModel.setCreatedby(lista.get(i).getData().get("Created By")+"");
                    petitionModel.setImagepath(lista.get(i).getData().get("ImagePath")+"");
                    petitionModel.setDescription(lista.get(i).getData().get("Description")+"");

                    petitionModel.setTotalComments((long)lista.get(i).getData().get("Comment"));

                    Log.i(TAG, "onComplete: "+lista.get(i).getData().get("Likes"));
                    if (!(lista.get(i).getData().get("Likes")+"").replaceAll("\\[","")
                            .replaceAll("\\]","").equals(""))
                    {
                        String[] likes=(lista.get(i).getData().get("Likes")+"").replaceAll("\\[","")
                                .replaceAll("\\]","")
                                .split(",");
                        petitionModel.setLikelist(Arrays.asList(likes));
                    }
                    else
                        petitionModel.setLikelist(new ArrayList<String>());




                    petitionModel.setUserLiked(petitionModel.getLikelist().contains(mAuth.getCurrentUser().getUid()));
                    petitionModel.setTotalLikes(petitionModel.getLikelist().size());
                    list.add(petitionModel);
                    lista.get(i).getData().get("Category");
                }
                adapter.notifyDataSetChanged();
            }
        });
    }
   }
