package com.example.alokitnigam.zersey_assign;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    Context context;
    ArrayList<CommentModel> list;
    private StorageReference mStorageRef;
    Bitmap bitmap;

    FirebaseAuth mAuth;
    public CommentsAdapter(Context c, ArrayList<CommentModel> list){
        this.list=list;
        this.context=c;
        mStorageRef = FirebaseStorage.getInstance().getReference("images");
        mAuth = FirebaseAuth.getInstance();
    }
    @NonNull
    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_view,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentsAdapter.ViewHolder holder, int position) {
        holder.comment.setText(list.get(position).comment);
        FirebaseFirestore.getInstance().collection("users")
                .document(list.get(position).getName())
                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        holder.name.setText(documentSnapshot.get("name")+"");
                    }
                });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder{

        TextView name,comment;
        public ViewHolder(View itemView) {
            super(itemView);
            name=itemView.findViewById(R.id.name);

            comment=itemView.findViewById(R.id.comment);
        }

    }

}
